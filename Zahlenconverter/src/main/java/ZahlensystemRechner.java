/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Moritz
 */
public class ZahlensystemRechner {

    public static void main(String[] args) {
        String binär1 = "101101";
        String binär2 = "100010";
        System.out.println(addiereBinär(binär1, binär2));
    }

    public static String addiereBinär(String binär1, String binär2) {
        /*0 + 0 = 0
    0 + 1 = 1 or 1 + 0 = 1
    1 + 1 = 10 */
        int tempo = 0;
        String res = "";

        for (int i = binär1.length() - 1; i >= 0; i--) {
            for (int j = binär2.length() - 1; j >= 0; j--) {
                tempo = binär1.charAt(i) + binär2.charAt(j);
                if (tempo > 1) {
                    tempo += tempo % 1;
                    res += tempo/1;
                    break;
                   
                } else {
                    res = tempo + res;
                    break;
                }

            }
        }

        return res;

    }

}
