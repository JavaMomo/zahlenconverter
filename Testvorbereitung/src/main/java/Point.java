/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Moritz
 */
public class Point {

  

    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean equality(Point m) {
        if (getX() == m.x && getY() == m.y) {
            return true;
        } else {
            return false;
        }
    }

    public String getPoint() {
        
        return "(" + x + "|" + y + ")";

    }

    public String getPoint(String s) {
  
        return s + "(" + x + "|" + y + ")";

    }

    public void spiegelnX() {
        y = y * -1;
    }

    public void spiegelnY() {
        x = x * -1;
    }
    public double distance(Point p2){
    

       return 0;
     
 }
}
