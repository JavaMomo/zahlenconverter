
import java.util.Date;
import java.util.ArrayList;

/**
 *
 * @author lukasprister
 */
public class Spieler extends Person {

    private String nickname;
    private ArrayList<Karte> spielerkarten;

    public Spieler(String nickname, String name, String nachname, Date Geburtsdatum) {
        super(name, nachname, Geburtsdatum);
        this.nickname = nickname;
        spielerkarten = new ArrayList<>();

    }

    public String getNickname() {
        return nickname;
    }

    private String getKarten() {
        String ret = "";
        for (int i = 0; i < spielerkarten.size(); i++) {
            ret += spielerkarten.get(i);
        }
        return ret;
    }

    public void addKarten(Karte e) {
        spielerkarten.add(e);
    }

    public int getPunkte() {
        int ret = 0;
        int AnzahlAss = 0;
        for (int i = 0; i < spielerkarten.size(); i++) {
            ret += spielerkarten.get(i).getWert().getZahlenwert();
            if (spielerkarten.get(i).getWert()==KartenWert.ASS) {
                AnzahlAss++;
            }
            
        }
        while (ret > 21 && AnzahlAss > 0) {
                ret -=10;
                AnzahlAss--;
        }
        return ret;
    }

    public ArrayList<Karte> getSpielerkarten() {
        return spielerkarten;
    }
     public void entferneAlleKarten(){
     spielerkarten.clear();
 }

    @Override
    public String toString() {
        return super.toString()+ getKarten();//To change body of generated methods, choose Tools | Templates.
    }

  

}
